package de.vaillant.errorhandling.valves;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ErrorReportValve;


public class CustomErrorReportValve extends ErrorReportValve
{

	@Override
	protected void report(Request request,Response response,Throwable trowable) {
		
		
		if(response.getStatus() < 400 || response.getContentWritten() > 0  || !response.isError()) {
			return;
		} else {
			try {
				response.setContentType("text/html");
				response.setCharacterEncoding("utf-8");
				response.getOutputStream().print(this.getErrorFileContent());
			} catch (IOException e) {
				//Log
			}
		}
		
	}
	
	
	private String getErrorFileContent() throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("503.html")));
		String line = null;
		while(null != (line = bufferedReader.readLine())) {
			sb.append(line);
		}
		
		return sb.toString();
	}
	
	
	
}
